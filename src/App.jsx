import { QRCode } from 'react-qrcode-logo';
import { useEffect, useState } from 'react';
import logo from './logo.svg';
import logoAndroid from './logo-android.svg';

const App = () => {
    let [options, setOptions] = useState();

    const setupQR = () => {
        const params = new URLSearchParams(window.location.search);
        const url = params.get('url') ?? 'https://oxygis.eu/';
        const variant = params.get('variant') ?? 'default';

        const variants = {
            default: {
                logo,
                logoWidth: 75,
            },
            android: {
                logo: logoAndroid,
                logoWidth: 100,
            },
        };

        const options = {
            value: url ?? 'https://oxygis.eu/',
            size: 400,
            fgColor: '#303FBE',
            logoImage: variants[variant]?.logo ?? logo,
            logoWidth: 200,
            logoHeight: variants[variant]?.height ?? 75,
            ecLevel: 'H',
            qrStyle: 'dots',
        };
        console.debug({ options });
        setOptions(options);
    };

    useEffect(() => {
        setupQR();
    }, []);

    return !!options && <QRCode {...options} />;
};

export default App;
